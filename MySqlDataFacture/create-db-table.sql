CREATE DATABASE  IF NOT EXISTS `newfacture`;

USE `newfacture`;

--
-- Table structure for table `table_facture`
--

DROP TABLE IF EXISTS `table_facture`;

CREATE TABLE `table_facture` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `n_facture` varchar(255) DEFAULT NULL,
  `fournisseur` varchar(255) DEFAULT NULL, 
  `imputation` varchar(255) DEFAULT NULL, 
  `montant` INT DEFAULT NULL, 
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;