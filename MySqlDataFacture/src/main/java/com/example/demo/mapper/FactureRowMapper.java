////
//package com.example.demo.mapper;
//
////this is the site wher foude the JDBC template
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import org.springframework.jdbc.core.RowMapper;
//
//import com.example.demo.model.Facture;
//
//public class FactureRowMapper implements RowMapper<Facture> {
//
//	@Override
//	public Facture mapRow(ResultSet rs, int rowNum) throws SQLException {
//		// instances objet facture
//		Facture facture = new Facture();
//
//		// populating a single domain object.
//		// getInt , getString built in function
//		facture.setId(rs.getInt("id"));
//		facture.setN_facture(rs.getString("n_facture"));
//		facture.setFournisseur(rs.getString("fournisseur"));
//		facture.setImputation(rs.getString("imputation"));
//		facture.setMontant(rs.getInt("montant"));
//
//		// return de the objet created for on record
//		return facture;
//	}
//}