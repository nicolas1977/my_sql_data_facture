//package com.example.demo.util;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.stereotype.Component;
//
//import com.example.demo.model.Facture;
//import com.example.demo.service.IFactureService;
//
//@Component
//public class DataLoader implements CommandLineRunner {
//
//	public DataLoader() {
//		System.out.println("je suis le constructeur du dataloader 4");
//
//	}
//
//	@Autowired
//	private IFactureService service;
//
//	// creating object instancing
//	/*
//	 * @Autowired private Facture facture;
//	 */
//	Facture facture = new Facture();
//
////	@RequestMapping("/getByFournisseur")
////	public Facture getByFournisseur() {
////		facture = facture.getByFournisseur("aftral");
////		System.out.println("passing for the RequestMapping");
////		return facture;
////	}
//
//	@Override
//	public void run(String... args) throws Exception {
//		System.out.println("------- the subroutine calling by StartSpringBoot ------");
//		// TRUNCATE TABLE TO STARTED TABEL VIDE
//		// service.truncTable();
//
//		// System.out.println("run override");
//		service.insertFacture(new Facture(345, "34562B", "altran", "batiment_D", 45000));
//		System.out.println("passe de ici insert facture records 1 ");
//
//		service.insertFacture(new Facture(346, "34563B", "afran", "batiment_A", 15000));
//		service.insertFacture(new Facture(347, "34524B", "alten", "batiment_B", 25000));
//		service.insertFacture(new Facture(386, "34164B", "sopra", "batiment_C", 35000));
//		System.out.println("passe de ici 2 ");
//		// testing for output records
//
//		// assignement the data facture from JDBC to object facture
//		// facture = facture.getByFournisseur("altran");
//
//		// Output in console de l'objet
//		System.out.println("the --------find by id is -------");
//		System.out.println(facture.toString());
//
//		service.getAllFactures();
//		System.out.println(service.getAllFactures());
//	}
//
//}
