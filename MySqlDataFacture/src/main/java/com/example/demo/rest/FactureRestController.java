package com.example.demo.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REFERENCY :
 * https://medium.com/voice-tech-podcast/create-a-rest-api-with-spring-boot-and-test-with-postman-42b7522f9013
 * 
 * @author d'aversa
 *
 **/

@RestController
@RequestMapping("/api")

public class FactureRestController {

	/**
	 * not using this private List<Facture> factures;
	 **/

//	@Autowired
//	IFactureService factureService;
//
//	// return a interely list of facture
//	@RequestMapping(value = "/getAllFactures", method = RequestMethod.GET)
//	public List<Facture> getAllFactures() {
//		// calling function in FactureService class
//		return factureService.getAllFactures();
//	}

	// return a interely list of facture
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String helloWorld() {
		// calling function in FactureService class
		return "ok je marche";
	}
}
