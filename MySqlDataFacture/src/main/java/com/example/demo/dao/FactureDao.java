//package com.example.demo.dao;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.stereotype.Repository;
//
//import com.example.demo.model.Facture;
//
/////Repository on entende une implementation en spring..
////est une decoration it extend the CRUD interface
////
//
//@Repository
//public class FactureDao implements IFactureDao {
//
//	@Autowired
//	private JdbcTemplate template;
//
//	/*********** SETTERS GETTERS ****************************/
//	// getter de ce proprieté instancié par AUTOWIRED
//	public JdbcTemplate getTemplate() {
//		return this.template;
//	}
//
//	// setter de ces propriete instancié par AUTOWIRED
//	public void setTemplate(JdbcTemplate template) {
//		this.template = template;
//	}
//
////	/******************** METHODES ****************************/
////	// methode #
////	@Override
////	public void truncTable() {
////		String sql = "TRUNCATE TABLE newfacture.table_facture";
////		// ok working the trucate table
////		template.update(sql);
////	}
//
//	// methode #1
//	@Override
//	public void insertFacture(Facture facture) {
//		String sql = "INSERT INTO table_facture(n_facture, fournisseur, imputation, montant) VALUES (?,?,?,?)";
//		Object[] parameter = new Object[] { facture.getN_facture(), facture.getFournisseur(), facture.getImputation(),
//				facture.getMontant() };
//		template.update(sql, parameter);
//		System.out.println("Updated Record with n_facture = " + facture.getN_facture());
//
//	}
//
//	@Override
//	public List<Facture> getAllFactures() {
//		String sql = "SELECT * FROM table_facture";
//		return getTemplate().query(sql, (rs, rowNum) -> new Facture(rs.getInt("id"), rs.getString("n_facture"),
//				rs.getString("fournisseur"), rs.getString("imputation"), rs.getInt("montant")));
//		// TODO Auto-generated method stub
//	}
//
////	@Override
////	public Facture getByFournisseur(String fournisseur) {
////		String sql = "SELECT facture FROM table_facture WHERE fournisseur = ?";
////		Object[] parameter = new Object[] { fournisseur };
////		FactureRowMapper rowMapper = new FactureRowMapper();
////		System.out.println("ok I m passing in function to output GetbyFornisseur");
////		// JdbcTemplate template = null;
////		return (Facture) getTemplate().queryForObject(sql, parameter, rowMapper);
////	}
//
//}
